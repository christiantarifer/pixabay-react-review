import React, { useState } from 'react';


// ************************************* CUSTOMIZED COMPONENTS ************************************ //

import Error from './Error';

// ************************************************************************************************ //

const Formulario = ({ guardarBusqueda }) => {

    const [ termino, guardarTermino ] = useState('');
    const [ error, guardarError ] = useState( false );

    const buscarImages = e => {

        e.preventDefault();

        // VALIDATE SEARCH COMPONENT
        if ( termino.trim() === '' ) {

            guardarError(true);
            return;

        }

        guardarError( false )

        // SEND SEARCH TEARM TO THE MAIN COMPONENT
        guardarBusqueda( termino );

    }

    return (

        <form onSubmit={buscarImages} >

            <div className="row">

                <div className="form-group col-md-8" >

                    <input 
                            type="text"
                            className="form-control form-control-lg"
                            placeholder="Buscar una imagen, ejemplo: fútbol o café"
                            onChange={ e => guardarTermino( e.target.value ) }
                    />

                </div>

                <div className="form-group col-md-4" >

                    <input 
                            type="submit"
                            className="btn btn-lng btn-danger btn-block"
                            value="Buscar"
                    />

                </div>

            </div>

            { error ? <Error mensaje="Agrega un termino de búsqueda" /> : null  }

        </form>

     );
}
 
export default Formulario;