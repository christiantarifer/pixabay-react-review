import React, { useState, useEffect } from 'react';


// ************************************* CUSTOMIZED COMPONENTS ************************************ //

import Formulario from './components/Formulario';
import ListadoImagenes from './components/ListadoImagenes';

// ************************************************************************************************ //
function App() {

  // APP STATE
  const [ busqueda, guardarBusqueda ] = useState('');
  const [ imagenes, guardarImagenes ] = useState( [] );
  const [ paginaactual, guardarPaginaActual ] = useState(1);
  const [ totalpaginas, guardarTotalPaginas ] = useState(1);

  useEffect( () => {

    const consultarApi = async () => {

        if( busqueda === '' ) return;

        const imagenesPorPagina = 30;

        const key = '20831385-7d9c26960795fbb72c014b290';

        const url = `https://pixabay.com/api/?key=${key}&q=${busqueda}&per_page=${imagenesPorPagina}&page=${paginaactual}`

        const respuesta = await fetch(url);

        const resultado = await respuesta.json();

        const { hits, totalHits } = resultado

        console.log(hits);

        guardarImagenes( hits );

        // CALCULATE TOTAL PAGES
        const calcularTotalPaginas = Math.ceil( totalHits / imagenesPorPagina );

        guardarTotalPaginas( calcularTotalPaginas );

        // MOVE SCREEN TO THE TOP
        const jumnbotron = document.querySelector('.jumbotron');

        jumnbotron.scrollIntoView( { behavior: 'smooth' } );

    }

    consultarApi();

    

  }, [ busqueda, paginaactual ]);

  // DEFINE PREVIOUS PAGE
    const paginaAnterior = () => {

      const nuevaPaginaActual = paginaactual - 1;

      if ( nuevaPaginaActual === 0 ) return;

      guardarPaginaActual(nuevaPaginaActual);

    }

  // DEFINE NEXT PAGE
  const paginaSiguiente = () => {

    const nuevaPaginaActual = paginaactual + 1;

    if ( nuevaPaginaActual > totalpaginas ) return;

    guardarPaginaActual(nuevaPaginaActual);

  }

  return (
    <div className="container">

      <div className="jumbotron">

        <p className="lead text-center" > Buscador de im&aacute;genes</p>

        <Formulario 
            guardarBusqueda={ guardarBusqueda }
        />

      </div>

      <div className="row justify-content-center" >
        <ListadoImagenes
          imagenes={ imagenes }
        />

        {

          ( paginaactual === 1 ) ? null : (
            <button
              type="button"
              className="btn btn-info mr-1"
              onClick={paginaAnterior}
            > &laquo; Anterior </button>
          ) 

        }

        {

          ( paginaactual === totalpaginas ) ? null : (

            <button
              type="button"
              className="btn btn-info mr-1"
              onClick={paginaSiguiente}
            > Siguiente &raquo;</button>

          )

        }

      </div>

    </div>
  );
}

export default App;
